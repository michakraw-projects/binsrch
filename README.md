**Zadanie
**

Masz daną posortowaną tablicę liczb nieujemnych nie dłuższą niż milion elementów. Dla każdego zapytania o liczbę x odpowiedz, ile liczb w tej tablicy jest nie mniejszych niż xi.

**Wejście**

W pierwszej linii dana jest liczba n (1 ≤ n ≤ 1000000), oznaczająca długość tablicy. Następnie dane jest n nieujemnych liczb w kolejności niemalejącej, które stanowią zawartość tablicy. W drugiej linii dana jest liczba m (1 ≤ m ≤ 1000000), oznaczająca ilość zapytań. Następnie danych jest m liczb xi.

**Wyjście
**

Dla każdego z m zapytań wypisz odpowiedź - ilość liczb nie mniejszych od liczby x z zapytania.

**Przykład**

Dla danych wejściowych

10 3 4 8 11 23 54 996 8710 911147 10001010

11 1 0 8 64 99 114 334 8484 41 911147 1000000000

poprawną odpowiedzią jest

10 10 8 4 4 4 4 3 5 2 0