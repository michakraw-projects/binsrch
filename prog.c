#include <cstdio>
#include <cstdlib>

int wyszukaj(int l, int p, int klucz, int tab[])
{
    int s = (p+l)/2;
    if(l>p) return l;
    if(tab[s]==klucz) 
    {
        while (tab[s-1] == klucz)
            s--;
        return s;
    }

    if(tab[s]>klucz) return wyszukaj(l,s-1,klucz,tab);
    else return wyszukaj(s+1,p,klucz,tab);
}


int main() 
{
    
    int n,m;
    scanf("%d",&n);
    int tab[n];
    for(int i=0;i<n;i++)
    {
        scanf("%d", &tab[i]);
    }
    scanf("%d",&m);
    int temp;
    for(int j=0;j<m;j++)
    {
        scanf("%d", &temp);
        if(temp>tab[n-1]) printf ("%d ", 0);
        else if(temp<tab[0]) printf("%d ", n);
        else printf("%d ", n-wyszukaj(0,n-1,temp,tab));
    }
    return 0; 
}
